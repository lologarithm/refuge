## Refuge
### Home Automation

This repo is for home automation systems. It is primarily designed around raspberry pi GPIO.


There are currently 3 primary binaries

1. cmd/refuge -- Central web server. Provide a --host=:XXXX to run the webserver. Web clients use a websocket to keep up to date. Web client will attempt to reconnect the socket. See './cmd/refuge/config.go' for configuration options. Loads from a file called 'config.json'
2. cmd/switch -- use name and a control pin. This is expected to be controlling a rely to turn a device on/off.
3. cmd/thermo -- used to control a thermostat. Expects a list of pins to control the heating/cooling sytem. Also expects a pin to read from a DHT22 temp/humidity sensor. Supports a motion detector pin as well to dynamically change temp.
4. cmd/garage -- used to control a garage but could be made more generic for any portal (doors/windows/etc). Currently just has 'current state' and the ability to set to open/closed. Need to add ability to lock/unlock to support house doors.

To build:

go build ./cmd/XXXX

To build a binary for use on a raspberry pi use env vars:
GOOS=linux
GOARCH=arm
GOARM=6 (for raspberry pi zero w)

Then you can upload the binary to the pi and run from there.

### Deployment
See ./scripts for example deployment scripts

Put refuge.service and deploy_xx.sh (renamed to deploy.sh) on each pi.
Run build_iot.sh and deploy_iot.sh to start each service.

Currently central server has go installed and uses `./scripts/deploy_refuge.sh` to build and restart that service.

### Short TODO List
Stats - nice to have graphs of the historical data.
