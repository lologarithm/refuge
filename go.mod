module gitlab.com/lologarithm/refuge

require (
	github.com/gorilla/websocket v1.2.0
	github.com/influxdata/influxdb-client-go v0.1.5
	github.com/influxdata/influxdb1-client v0.0.0-20191209144304-8bf82d3c094d
	github.com/lologarithm/netgen v1.1.0
	github.com/mailgun/mailgun-go/v3 v3.3.0
	github.com/stianeikeland/go-rpio v4.2.0+incompatible
)

go 1.13
