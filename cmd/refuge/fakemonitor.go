package main

import (
	"time"

	"gitlab.com/lologarithm/refuge/refuge"
	"gitlab.com/lologarithm/refuge/rnet"
)

func fakeMonitor() chan rnet.Msg {
	tstream := make(chan rnet.Msg, 10)
	go func() {
		i := 0
		for {
			tstream <- rnet.Msg{
				Device: &refuge.Device{
					Name:        "Test Living Room",
					ID:          "test_living_room",
					Addr:        "192.168.1.121:1234",
					Thermostat:  &refuge.Thermostat{Target: 23.5, State: refuge.StateCooling, Settings: refuge.Settings{High: 25, Low: 18, Mode: refuge.ModeAuto}},
					Thermometer: &refuge.Thermometer{Temp: 25 + (float32(i % 3)), Humidity: 10.1},
				},
			}
			time.Sleep(3 * time.Second)
			dev := &refuge.Device{
				Name:        "Test Family Room",
				ID:          "test_family_room",
				Addr:        "192.168.1.122:1234",
				Thermostat:  &refuge.Thermostat{Settings: refuge.Settings{High: 26, Low: 18, Mode: refuge.ModeAuto}},
				Thermometer: &refuge.Thermometer{Temp: 17 + (float32(i % 3)), Humidity: 10.1},
			}
			if dev.Thermometer.Temp < dev.Thermostat.Settings.Low {
				dev.Thermostat.State = refuge.StateHeating
				dev.Thermostat.Target = 21.5
			}
			tstream <- rnet.Msg{
				Device: dev,
			}
			time.Sleep(3 * time.Second)
			tstream <- rnet.Msg{
				Device: &refuge.Device{
					Name:   "Test Fireplace",
					ID:     "test_fireplace",
					Addr:   "192.168.1.123:1234",
					Switch: &refuge.Switch{On: i%2 == 0},
				},
			}
			time.Sleep(3 * time.Second)
			tstream <- rnet.Msg{
				Device: &refuge.Device{
					Name: "Test Garage Door",
					ID:   "test_garage_door",
					Addr: "192.168.1.124:1234",
					Portal: &refuge.Portal{
						State: refuge.PortalState(i%2 + 1),
					},
				},
			}
			time.Sleep(3 * time.Second)
			i++
		}
	}()

	return tstream
}
