package main

import (
	"encoding/json"
	"log"
	"os"
)

var globalConfig Config

// Config is server configuration.
// Includes users&access levels as well as Mailgun config to send warning emails.
type Config struct {
	Users         map[string]userAccess
	Mailgun       MailgunConfig
	NetPrefix     string
	Location      string
	DarkSkyAPIKey string
	WeatherEmail  string
}

// MailgunConfig is the settings needed to use Mailgun for emails.
type MailgunConfig struct {
	APIKey     string
	Domain     string
	Sender     string
	Recipients []string
}

func loadUserConfig() {
	globalConfig = Config{
		Users: map[string]userAccess{},
	}
	data, err := os.ReadFile("config.json")
	if err == nil {
		jerr := json.Unmarshal(data, &globalConfig)
		if jerr != nil {
			log.Printf("Failed to unmarshal: %#v", jerr)
		}
		for name, v := range globalConfig.Users {
			log.Printf("User: %s, Access: %d", name, v.Access)
		}
		log.Printf("Mailgun Config:\n\t%#v", globalConfig.Mailgun)
	} else {
		log.Printf("Failed to open config: %v", err)
		if blankData, err := json.Marshal(globalConfig); err == nil {
			if err := os.WriteFile("config.json", blankData, 0644); err != nil {
				log.Printf("Failed to write blank config: %v", err)
			}
		} else {
			log.Printf("Failed to serialize blank config: %v", err)
		}
	}
}
