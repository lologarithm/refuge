import 'package:flutter/material.dart';
import 'package:refuge/settings.dart';
import 'house_widget.dart';
import 'family_widget.dart';
import 'computer_widget.dart';
import 'devices.dart';

class RootPage extends StatefulWidget {
  const RootPage({super.key, required this.title});
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<RootPage> createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  _RootPageState();

  late List<Widget> _pages;

  @override
  void initState() {
    super.initState();
    _pages = <Widget>[
      const HouseWidget(title: "House"),
      const FamilyWidget(title: "Family"),
      const ComputerWidget(title: "Computer"),
      const SettingsWidget(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // TRY THIS: Try changing the color here to a specific color (to
        // Colors.amber, perhaps?) and trigger a hot reload to see the AppBar
        // change color while the other colors stay the same.
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        // Here we take the value from the RootPage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: IndexedStack(
        index: _selectedIndex,
        children: _pages,
      ),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
          type: BottomNavigationBarType.fixed,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.house),
              label: "House",
              activeIcon: Icon(Icons.house,
                  color: Theme.of(context).colorScheme.primary),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: "Family",
              activeIcon: Icon(Icons.person,
                  color: Theme.of(context).colorScheme.primary),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: "Computers",
              activeIcon: Icon(Icons.person,
                  color: Theme.of(context).colorScheme.primary),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings_sharp),
              label: "Config",
              activeIcon: Icon(Icons.settings_sharp,
                  color: Theme.of(context).colorScheme.primary),
            ),
          ]),
    );
  }

  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
