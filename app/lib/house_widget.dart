import 'dart:collection';
import 'package:flutter/material.dart';
import 'devices.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'dart:io';
import 'dart:async';
import 'dart:typed_data';
import 'dart:convert';
import 'package:intl/intl.dart';

class HouseWidget extends StatefulWidget {
  const HouseWidget({super.key, required this.title});
  final String title;
  @override
  State<HouseWidget> createState() => _HouseWidgetState();
}

class _HouseWidgetState extends State<HouseWidget> {
  final Map<String, DeviceUpdate> devices = HashMap<String, DeviceUpdate>();
  final Map<String, DeviceUpdate> pendingDevUpdates = HashMap<String, DeviceUpdate>();
  _HouseWidgetState();
  final StreamController<DeviceUpdate> sc = StreamController();
  int delay = 1000;
  String socketState = "disconnected";
  Socket? netSock;
  String user = "echols";
  String pass = "familyrefuge";
  String host = 'echols.io';
  int port = 6907;

  @override
  initState() {
    super.initState();
    var net = sc.stream;
    setupSocket();

    try {
      net.listen((event) {
        if (event.id == "") {
          event.id = event.name;
        }
        setState(() {
          devices[event.id] = event;
        });
      });
    } catch (e) {
      print("failed to listen...$e");
    }
  }

  void setupSocket() {
    // max delay of 1 minute
    if (delay > 60000) {
      delay = 60000;
    }

    if (socketState == "connected") {
      return;
    }

    try {
      print("starting connect to $host:$port now....");
      Socket.connect(host, port).then((sock) {
        print("connection completed.");
        setState(() {
          netSock = sock;
          socketState = "connected";
          delay = 1000;
        });

        sock.listen(
          (Uint8List data) {
            final updates = String.fromCharCodes(data).split("\n");
            for (var uptxt in updates) {
              if (uptxt.contains("error")) {
                socketState = uptxt;
                return;
              }
              if (uptxt.length < 2) {
                continue;
              }
              var idx = uptxt.indexOf("}{");
              while (idx > 0) {
                // print("server multi-part ${uptxt.substring(0, idx + 1)}");
                sc.add(DeviceUpdate.fromJson(jsonDecode(uptxt.substring(0, idx + 1))));
                uptxt = uptxt.substring(idx + 1);
                // handle when there is no newline between records.
                idx = uptxt.indexOf("}{");
              }
              // print("server msg: $uptxt");
              sc.add(DeviceUpdate.fromJson(jsonDecode(uptxt)));
            }
          },
          onError: (error) {
            socketState = error.toString();
            Future.delayed(Duration(milliseconds: delay), () {
              setState(() {
                delay *= 2;
                setupSocket();
              });
            });
          },
          onDone: () {
            socketState = "disconnected";
            Future.delayed(Duration(milliseconds: delay), () {
              setState(() {
                delay *= 2;
                setupSocket();
              });
            });
          },
        );
        print('Connected to: ${sock.remoteAddress.address}:${sock.remotePort}');
        sock.write('{"user": "$user", "pass": "$pass"}');
      }).onError((error, stackTrace) {
        print("connection failed ${error.toString()} - retrying in ${delay / 1000} seconds.");
        Future.delayed(Duration(milliseconds: delay), () {
          setState(() {
            delay *= 2;
            setupSocket();
          });
        });
      });
    } catch (e) {
      print("connection failed ${e.toString()} - retrying in ${delay / 1000} seconds.");
      Future.delayed(Duration(milliseconds: delay), () {
        setState(() {
          delay *= 2;
          setupSocket();
        });
      });
    }
  }

  changeComplete() {
    // commit to network pending change.
    setState(() {
      pendingDevUpdates.forEach((key, value) {
        var req = UpdateRequest(value.name);
        var old = devices[key]!;
        if (old.portal != null) {
          req.toggle = value.portal!.state.index;
        } else if (old.thermostat != null) {
          req.climate = value.thermostat;
        } else if (old.switchable != null) {
          req.toggle = value.switchable!.on ? 1 : 0;
        }
        value.pending = true;
        devices[key] = value;

        var msg = jsonEncode(req);
        if (netSock != null) {
          netSock!.write(msg);
        }
      });
      pendingDevUpdates.clear();
    });
  }

  changeDevice(DeviceUpdate up) {
    setState(() {
      pendingDevUpdates[up.id] = up;
    });
  }

  @override
  Widget build(BuildContext context) {
    var devWidgets = <Widget>[];
    if (socketState != "connected") {
      devWidgets.add(Card(
          color: Colors.red.shade300,
          child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.start, children: [
            const Text("NOT CONNECTED TO SERVER."),
            const Text("DATA CAN BE STALE"),
            Text("($socketState)"),
            TextButton(
              child: const Text("Reconnect Now"),
              onPressed: () => setupSocket(),
            ),
          ])));
    }
    devices.forEach((key, value) {
      if (pendingDevUpdates.containsKey(key)) {
        DeviceWidget dw = DeviceWidget(
          dev: pendingDevUpdates[key]!,
          changeComplete: changeComplete,
          deviceChange: changeDevice,
        );
        devWidgets.add(dw);
      } else {
        DeviceWidget dw = DeviceWidget(
          dev: value,
          changeComplete: changeComplete,
          deviceChange: changeDevice,
        );
        devWidgets.add(dw);
      }
    });

    // This method is rerun every time setState is called
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return SingleChildScrollView(
      child: Column(
        // Column is also a layout widget. It takes a list of children and
        // arranges them vertically. By default, it sizes itself to fit its
        // children horizontally, and tries to be as tall as its parent.
        //
        // Column has various properties to control how it sizes itself and
        // how it positions its children. Here we use mainAxisAlignment to
        // center the children vertically; the main axis here is the vertical
        // axis because Columns are vertical (the cross axis would be
        // horizontal).
        //
        // TRY THIS: Invoke "debug painting" (choose the "Toggle Debug Paint"
        // action in the IDE, or press "p" in the console), to see the
        // wireframe for each widget.
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: devWidgets,
      ),
    );
  }
}

class DeviceWidget extends StatelessWidget {
  const DeviceWidget({super.key, required this.dev, required this.changeComplete, required this.deviceChange});

  final DeviceUpdate dev;
  final Function changeComplete;
  final Function(DeviceUpdate) deviceChange;

  @override
  Widget build(BuildContext context) {
    var column = Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          dev.name,
          textScaleFactor: 1.5,
        )
      ],
    );
    var tempColor = Theme.of(context).hintColor;
    if (dev.thermostat != null) {
      if (dev.thermostat!.state == ThermostatState.cool) {
        tempColor = Colors.blue;
      } else if (dev.thermostat!.state == ThermostatState.cool) {
        tempColor = Colors.red;
      }
    }
    if (dev.thermometer != null) {
      column.children.add(Row(children: [
        const Spacer(flex: 10),
        Text("${cToF(dev.thermometer!.temp).toStringAsFixed(1)}F"),
        Icon(Icons.thermostat, color: tempColor),
        const Spacer(flex: 1),
        Text("${(dev.thermometer!.humidity).toStringAsFixed(1)}%"),
        const Icon(Icons.water_drop_outlined, color: Colors.blue),
        const Spacer(flex: 10),
      ]));
    }
    if (dev.thermostat != null) {
      column.children.add(Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
        const Spacer(flex: 1),
        Flexible(
            flex: 15,
            child: SfLinearGauge(
                // radiusFactor: 0.4,
                minimum: 50,
                maximum: 100,
                showTicks: false,
                ranges: <LinearGaugeRange>[
                  LinearGaugeRange(
                      startValue: cToF(dev.thermostat!.low),
                      endValue: cToF(dev.thermostat!.high),
                      position: LinearElementPosition.cross,
                      color: Colors.grey),
                ],
                markerPointers: <LinearMarkerPointer>[
                  LinearWidgetPointer(
                    value: cToF(dev.thermostat!.low),
                    position: LinearElementPosition.outside,
                    onChangeEnd: (value) {
                      changeComplete();
                    },
                    onChanged: (value) {
                      value = fToC(value);
                      if (value > dev.thermostat!.high - 2) {
                        return;
                      }
                      DeviceUpdate up = DeviceUpdate.clone(dev);
                      up.thermostat!.low = value;
                      deviceChange(up);
                    },
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(cToF(dev.thermostat!.low).toStringAsFixed(1)),
                        const Icon(Icons.location_pin, color: Colors.red, size: 30),
                      ],
                    ),
                  ),
                  LinearWidgetPointer(
                    onChangeEnd: (value) {
                      changeComplete();
                    },
                    onChanged: (value) {
                      value = fToC(value);
                      if (value < dev.thermostat!.low + 2) {
                        return;
                      }
                      DeviceUpdate up = DeviceUpdate.clone(dev);
                      up.thermostat!.high = value;
                      deviceChange(up);
                    },
                    value: cToF(dev.thermostat!.high),
                    enableAnimation: true,
                    dragBehavior: LinearMarkerDragBehavior.free,
                    position: LinearElementPosition.outside,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(cToF(dev.thermostat!.high).toStringAsFixed(1)),
                        const Icon(Icons.location_pin, color: Colors.blue, size: 30),
                      ],
                    ),
                  ),
                  LinearShapePointer(
                    value: cToF(dev.thermometer!.temp),
                    shapeType: LinearShapePointerType.diamond,
                    position: LinearElementPosition.cross,
                    color: tempColor,
                  )
                ])),
        const Spacer(
          flex: 1,
        ),
        Switch(
            value: dev.thermostat!.mode == ThermostatRunMode.auto,
            onChanged: (bool state) {
              DeviceUpdate up = DeviceUpdate.clone(dev);
              if (state) {
                up.thermostat!.mode = ThermostatRunMode.auto;
              } else {
                up.thermostat!.mode = ThermostatRunMode.off;
              }
              deviceChange(up);
              changeComplete();
            })
      ]));
    }

    if (dev.portal != null) {
      column.children.add(IconButton(
          onPressed: () {
            DeviceUpdate up = DeviceUpdate.clone(dev);
            if (dev.portal!.state == PortalState.closed) {
              up.portal!.state = PortalState.open;
            } else {
              up.portal!.state = PortalState.closed;
            }
            deviceChange(up);
            changeComplete();
          },
          icon: dev.portal!.state == PortalState.closed
              ? const Icon(Icons.door_sliding, size: 50)
              : const Icon(Icons.door_sliding_outlined, color: Colors.red, size: 50)));
    }

    if (dev.switchable != null) {
      if (dev.name.contains("fire") || dev.name.contains("Fire")) {
        column.children.add(IconButton(
            onPressed: () {
              DeviceUpdate up = DeviceUpdate.clone(dev);
              up.switchable!.on = !dev.switchable!.on;
              deviceChange(up);
              changeComplete();
            },
            icon: dev.switchable!.on
                ? const Icon(
                    Icons.fireplace_outlined,
                    color: Colors.orange,
                    size: 50,
                  )
                : const Icon(
                    Icons.fireplace_outlined,
                    size: 50,
                  )));
      } else {
        column.children.add(Switch(
            value: dev.switchable!.on,
            onChanged: (bool state) {
              DeviceUpdate up = DeviceUpdate.clone(dev);
              up.switchable!.on = state;
              deviceChange(up);
              changeComplete();
            }));
      }
    }
    DateFormat formatter = DateFormat('yyyy-MM-dd HH-mm-ss');
    column.children.add(Text(
      "Last Update: ${formatter.format(dev.lastUpdate)}",
      style: const TextStyle(fontSize: 9),
    ));

    List<Widget> content = [
      Center(child: column),
    ];
    if (dev.pending) {
      // content.add(const Opacity(
      //   opacity: 0.8,
      //   child: ModalBarrier(
      //       dismissible: true,
      //       color: Colors.black,
      //       semanticsOnTapHint: "dismiss pending change."),
      // ));
      content.add(const Center(
        child: CircularProgressIndicator(),
      ));
    }
    return Card(child: Center(child: Stack(fit: StackFit.passthrough, children: content)));
  }
}

double cToF(double c) {
  return (c * 1.8) + 32;
}

double fToC(double f) {
  return (f - 32) * 0.5556;
}
